package one.njk.celestidesk.network.auth.model

data class TokenResponse (
    val message: String,
    val token: String,
)